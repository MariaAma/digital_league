package peopleData;

public class Person {
    private String familiya;
    private String imya;
    private String otchestvo;
    private Integer age;
    private String gender;
    private String birthdate;

    public Person(String imya, String otchestvo, String familiya, Integer age, String gender, String birthdate) {
        this.familiya = familiya;
        this.imya = imya;
        this.otchestvo = otchestvo;
        this.age = age;
        this.gender = gender;
        this.birthdate = birthdate;
    }

    public boolean equals(Person newPerson) {
        return this.getImya().equals(newPerson.getImya()) ||
                this.getFamiliya().equals(newPerson.getFamiliya()) ||
                this.getOtchestvo().equals(newPerson.getOtchestvo()) ||
                this.getBirthdate().equals(newPerson.getBirthdate());
    }
    public String toString() {
        return this.getFamiliya() + " " +
                this.getImya() + " " +
                this.getOtchestvo() + ", " +
                this.getAge() + " лет, " +
                this.getGender() + ", " +
                this.getBirthdate();
    }
    public String getFamiliya() {
        return familiya;
    }

    public String getImya() {
        return imya;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthdate() {
        return birthdate;
    }
}

