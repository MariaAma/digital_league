package peopleData;

import java.util.Arrays;
import java.util.List;

public class PeopleList {
    private static Person[] peopleArray = new Person[] {
            new Person("Анна", "Михайловна", "Иванова", 28, "жен", "19.10.1992"),
            new Person("Елена", "Васильевна", "Смирнова", 25, "жен", "11.03.1995"),
            new Person("Игорь", "Семёнович", "Богданов", 17, "муж", "31.01.2003"),
            new Person("Игорь", "Семёнович", "Богданов", 17, "муж", "31.01.2003"),
            new Person("Игорь", "Семёнович", "Богданов", 17, "муж", "31.01.2003"),
            new Person("Елена", "Васильевна", "Смирнова", 25, "жен", "11.03.1995"),
            new Person("Анатолий", "Сергеевич", "Алексеев", 19, "муж", "13.06.2001"),
            new Person("Анатолий", "Сергеевич", "Алексеев", 19, "муж", "13.06.2001"),
            new Person("Артур", "Вячеславович", "Московский", 53, "муж", "07.09.1967"),
    };
    public static List<Person> peopleList = Arrays.asList(peopleArray);

}
