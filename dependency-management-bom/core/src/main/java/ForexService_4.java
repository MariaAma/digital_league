import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static connection.Connection.getUrlContent;

public class ForexService_4 {

    public static double calculateCurrency(String initialSum, String currencyFrom, String currencyTo) {
        String output = getUrlContent(
                "https://api.exchangeratesapi.io/latest?base=" + currencyFrom);
        int i = output.indexOf(currencyTo);
        String price = output.substring(i+5, output.indexOf(",", i));
        return Double.parseDouble(initialSum) * Double.parseDouble(price);
    }

    public static void main(String[] args) {
        String currencyFrom="", currencyTo="", initSum = "";
        System.out.println("Список валют: \nUSD, EUR, RUB, HKD, ISK, PHP, DKK, HUF, CZK, \n"
                + "AUD, RON, SEK, IDR, INR, BRL, HKD, HRK, JPY, \nTHB, CHF, SGD, PLN, BGN, "
                + "TRY, CNY, NOK, NZD, \nZAR, CAD, MXN, ILS, GBP, KRW, MYR, CAD\n");
        System.out.println("Выберите начальную валюту: ");
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            currencyFrom = bufReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Выберите валюту для перевода: ");
        try {
            currencyTo = bufReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Введите сумму: ");
        try {
            initSum = bufReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Минутку...");
        double resultSum = calculateCurrency(initSum, currencyFrom, currencyTo);
        System.out.format(currencyTo + ": %.2f\n", resultSum);
    }
}

