import peopleData.Person;
import static peopleData.PeopleList.peopleList;


public class FindPersonInList_1 {
    public static void main(String[] args) {
        int foundMatches = 0;
        Person newPerson = new Person("Юрий", "Александрович", "Третьяков",
                24, "муж", "30.03.1996");
        foundMatches = (int) peopleList.stream().filter(person -> person.equals(newPerson)).count();
        if (foundMatches > 0)
            System.out.println("Found matches: " + foundMatches);
        else
            System.out.println("No match found");
   }
}
