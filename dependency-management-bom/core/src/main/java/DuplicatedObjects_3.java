import peopleData.Person;

import java.util.*;
import static peopleData.PeopleList.peopleList;

public class DuplicatedObjects_3 {
    public static void main(String[] args) {

        List<Person> hasDuplicates = new ArrayList<>();
        List<Person> hasMore3Duplicates = new ArrayList<>();

        Collections.sort(peopleList, Comparator.comparing(Person::toString));
        for (int i=0; i<peopleList.size();i++) {
            Person searchedPerson = peopleList.get(i);
            int numOfDuplicates =
                    (int) peopleList.stream().filter(person -> person.equals(searchedPerson)).count();

            if (numOfDuplicates==2 || numOfDuplicates==3)
                hasDuplicates.add(searchedPerson);
            else if (numOfDuplicates>3)
                hasMore3Duplicates.add(searchedPerson);
            i+=(numOfDuplicates-1);
        }
        Collections.sort(hasDuplicates, Comparator.comparingInt(Person::getAge));
        for(Person person:hasDuplicates)
            System.out.println("{" + (hasDuplicates.indexOf(person) + 1) + ":{" + person + "}}");
    }
}
