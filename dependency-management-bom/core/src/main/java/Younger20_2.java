import static peopleData.PeopleList.peopleList;

public class Younger20_2 {
    public static void main(String[] args) {

        peopleList.stream().filter(person -> person.getAge()<=20)
                .filter(person -> person.getFamiliya().toLowerCase().charAt(0)=='а'
                || person.getFamiliya().toLowerCase().charAt(0)=='б'
                || person.getFamiliya().toLowerCase().charAt(0)=='в'
                || person.getFamiliya().toLowerCase().charAt(0)=='г'
                || person.getFamiliya().toLowerCase().charAt(0)=='д')
                .forEach(System.out::println);
    }
}
